# Pocketbook 614 (PB-E614.4.4.1774) Modifications

1. Added ability to perform `dosfsck` on `/dev/user_int` on startup
2. Added ability to trigger `/lib/modules/ins_usbnet.sh` on startup
3. Added ability to configure USB network with GW and DNS on demand
4. Other workarounds and aesthetic changes in shell scripts as well