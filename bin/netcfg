#!/bin/sh

# Networking configuration script
# Set/get DNS and default gateway

export PATH=${PATH}:/ebrmain/xbin

help () {
    cat << EOF
 USAGE: /bin/netcfg [OPTIONS]
 OPTIONS:
 -g          Get
 -s          Set
 -d          Del
 -p          <param>: DNS, GW
 -v          <value>
EOF
}

while getopts "hgdsp:v:" OPT; do
    case "${OPT}" in
        "g" ) ACTION="GET" ;;
        "s" ) ACTION="SET" ;;
        "d" ) ACTION="DEL" ;;
        "p" ) PARAM="${OPTARG}" ;;
        "v" ) VALUE="${OPTARG}" ;;
        "h" ) help; exit 0; ;;
    esac
done

if [ -z "${PARAM}" ]; then echo "ERROR: Missed parameter; run with -h to see help"; exit 1; fi

if [ ! -f "/etc/netcfg.conf" ]; then
    echo "WARNING: '/etc/netcfg.conf' does not exist"
else
    . /etc/netcfg.conf
fi

case "${PARAM}" in
    "DNS" ) 
        case "${ACTION}" in
            "GET" )
                cat /etc/resolv.conf
            ;;
            "SET" )
                if [ ! -z "${VALUE}" ]; then
                    DNS="${VALUE}"
                fi
                if [ -z "${DNS}" ]; then
                    echo "ERROR: At least 1 server should be provided, exiting with errcode 1"
                    exit 1 
                fi
                cat /dev/null > /etc/resolv.conf
                for SERVER in ${DNS}; do echo "nameserver ${SERVER}" >> /etc/resolv.conf; done
            ;;
            "DEL" )
                rm /etc/resolv.conf
            ;;
        esac
    ;;
    "GW" )
        case "${ACTION}" in
            "GET" )
                ip route show
            ;;
            "SET" )
                if [ ! -z "${VALUE}" ]; then
                    GW="${VALUE}"
                fi
                if [ -z "${GW}" ]; then
                    echo "ERROR: No default gateway has been provided, exiting with errcode 1"
                    exit 1
                fi               
                ip route add default via "${GW}"
            ;;
            "DEL" )
                ip route flush default
            ;;
        esac
    ;;
esac
