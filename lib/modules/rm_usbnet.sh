#!/bin/sh

# Declare some variables

IFACE="rndis0"
ANDROID_USB="/sys/class/android_usb/android0"

# Kill both Dropbear && DHCP daemon processes

killall dropbear
killall udhcpd

# Tell the driver to disable 'USB Networking'

echo "0" > ${ANDROID_USB}/enable

exit 0
