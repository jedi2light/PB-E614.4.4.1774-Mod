#!/bin/sh

# Declare some variables

ANDROID_USB="/sys/class/android_usb/android0"
CPU_GOVERNOR_CUR="/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
CPU_GOVERNOR_BAC="/var/run/last_scaling_governor"

# Disable USB Mass Storage as we gonna use Android driver for Networking

echo "0" > ${ANDROID_USB}/enable

# Restore CPU governor

cat ${CPU_GOVERNOR_BAC} > ${CPU_GOVERNOR_CUR}

# Sleep for one second, then invoke external script to enable Networking

sleep 1; /lib/modules/ins_usbnet.sh

exit 0
