#!/bin/sh

# This hack is required to override applications && system directories hide

case $1 in
  query )
      ISCON=`cat /sys/devices/platform/sw_usb_udc/is_connected 2>/dev/null`
      if [ "$ISCON" == "1" ]; then exit 99; fi
      exit 0
      ;;
esac
