#!/bin/sh

# Declare some variables

ANDROID_USB="/sys/class/android_usb/android0"
CPU_GOVERNOR_CUR="/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
CPU_GOVERNOR_BAC="/var/run/last_scaling_governor"

# Disable USB Networking as we gonna use Android driver for Mass Storage

/lib/modules/rm_usbnet.sh; sleep

# Backup CPU governor

cat ${CPU_GOVERNOR_CUR} > ${CPU_GOVERNOR_BAC}

# Set CPU governor

echo "performance" > ${CPU_GOVERNOR_CUR}

# Set up USB Mass Storage functionality
# Present device as Obreey Pocketbook 614
# And share only internal vFAT32 partition (by default) with user's data

echo "PocketBook_614" > ${ANDROID_USB}/iProduct
echo "Obreey" > ${ANDROID_USB}/iManufacturer
cat /var/run/serial > ${ANDROID_USB}/iSerial
echo "mass_storage" > ${ANDROID_USB}/functions
echo "/dev/mmcblk0p1" > ${ANDROID_USB}/f_mass_storage/lun/file

# If external SD card is inserted, provide access to it via Mass Storage

LUN1=""
if grep -q mmcblk1p1 /proc/partitions; then
	LUN1="/dev/mmcblk1p1"
fi

echo ${LUN1} > ${ANDROID_USB}/f_mass_storage/lun1/file

# Tell the driver to enable configured earlier MassStorage functionality

echo "1" > ${ANDROID_USB}/enable

exit 0
