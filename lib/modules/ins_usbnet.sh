#!/bin/sh

# Declare some variables

NET="192.168.205"
IFACE="rndis0"
DHCP_CONF="/var/run/udhcpd.conf"
ANDROID_USB="/sys/class/android_usb/android0"

# Set up USB Networking
# Present device as Obreey Pocketbook
# Also set the idVendor && idProduct USB parameters

echo "0" > ${ANDROID_USB}/enable
echo "rndis" > ${ANDROID_USB}/functions
echo "Obreey" > ${ANDROID_USB}/iManufacturer
echo "Obreey" > ${ANDROID_USB}/f_rndis/manufacturer
echo "Pocketbook" > ${ANDROID_USB}/iProduct
echo "04b3" > ${ANDROID_USB}/idVendor
echo "4010" > ${ANDROID_USB}/idProduct

# Enable USB Networking, then sleep for 1/4 seconds

echo "1" > ${ANDROID_USB}/enable
usleep 250000

# Set up loop back and USB Networking interfaces

ifconfig lo up
ifconfig ${IFACE} up ${NET}.1

# Set up DHCP server with in-memory configuration

cat << EOF > ${DHCP_CONF}
start ${NET}.2
end ${NET}.255
interface ${IFACE}
opt subnet 255.255.255.0
EOF

# Start DHCP and Dropbear, a lightweight SSH server

/sbin/udhcpd ${DHCP_CONF}
/sbin/dropbear -G ${NET}

exit 0
